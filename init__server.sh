#!/bin/sh
export DEBIAN_FRONTEND=noninteractive;

echo "====================================";
echo "Hello";
echo "This is Iggy Autorave and I'm about to install your webserver";  
echo "====================================";

echo "America/Mexico_City" > /etc/timezone; 
unlink /etc/localtime && dpkg-reconfigure -f noninteractive tzdata;

apt update -y; apt upgrade -y && apt-get autoremove -y && apt-get autoclean -y;

echo "====================================";
echo "Installing Vim latest Version";  
echo "====================================";

add-apt-repository -y ppa:jonathonf/vim && apt update -y && apt install vim -y;

echo "====================================";
echo "Installing Git";  
echo "====================================";

apt install git -y;

echo "====================================";
echo "Installing ZSH";  
echo "====================================";

apt install zsh -y && chsh -s /bin/zsh;

echo "===================================="; 
echo "Installing Antigen";  
echo "====================================";

if [ -d ~/.antigen ]
then 
    rm -rf .antigen;
    git clone https://github.com/zsh-users/antigen.git ~/.antigen;
else
    git clone https://github.com/zsh-users/antigen.git ~/.antigen;
fi

echo "====================================";
echo "I will configure Antigen for you";  
echo "====================================";

if [ -f ~/.zshrc ]
then 
  rm -rf .zshrc;

echo "# .zshrc Source Basic
source '${HOME}/.antigen/antigen.zsh'

# Load the oh-my-zsh's library.
antigen use oh-my-zsh

# Bundles from the default repo (robbyrussell's oh-my-zsh).
antigen bundle git
antigen bundle git-flow
antigen bundle rsync
antigen bundle pip
antigen bundle python
antigen bundle rails
antigen bundle rake-fast
antigen bundle ruby
antigen bundle history
antigen bundle command-not-found

# Third Party
antigen bundle kennethreitz/autoenv
antigen bundle zsh-users/zsh-completions

# Syntax highlighting bundle.
antigen bundle zsh-users/zsh-completions src
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-history-substring-search

# Load the theme.
antigen theme https://github.com/denysdovhan/spaceship-zsh-theme spaceship

# Tell antigen that you're done.
antigen apply

# Condition a Reboot.

source '${HOME}/.aliases' " >> ~/.zshrc;

else
echo "# .zshrc Source Basic
source '${HOME}/.antigen/antigen.zsh'

# Load the oh-my-zsh's library.
antigen use oh-my-zsh

# Bundles from the default repo (robbyrussell's oh-my-zsh).
antigen bundle git
antigen bundle git-flow
antigen bundle rsync
antigen bundle pip
antigen bundle python
antigen bundle rails
antigen bundle rake-fast
antigen bundle ruby
antigen bundle history
antigen bundle command-not-found

# Third Party
antigen bundle kennethreitz/autoenv
antigen bundle zsh-users/zsh-completions

# Syntax highlighting bundle.
antigen bundle zsh-users/zsh-completions src
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-history-substring-search

# Load the theme.
antigen theme https://github.com/denysdovhan/spaceship-zsh-theme spaceship

# Tell antigen that you're done.
antigen apply

# Condition a Reboot.

source '${HOME}/.aliases' " >> ~/.zshrc;
fi

echo "====================================";
echo "Antigen Configurations Done"; 
echo "====================================";

echo "====================================";
echo "Let me Set the Aliases"; 
echo "====================================";

if [ -f ~/.aliases ]
then 
 rm -rf ~/.aliases;

echo "#Set Aliases
alias gonginx='/etc/nginx/'
alias gomysql='/etc/mysql/'
alias www='/var/www/'
alias zshrc='vim ~/.zshrc'
alias aliax='vim ~/.aliases'
alias vimx='vim ~/.vimrc'" >> ~/.aliases;

else
echo "#Set Aliases
alias gonginx='/etc/nginx/'
alias gomysql='/etc/mysql/'
alias www='/var/www/'
alias zshrc='vim ~/.zshrc'
alias aliax='vim ~/.aliases'
alias vimx='vim ~/.vimrc'" >> ~/.aliases;
fi

echo "====================================";
echo "Aliases was Set"; 
echo "====================================";

echo "====================================";
echo "Let me configure SSH to a different Port"; 
echo "====================================";

cp /etc/ssh/sshd_config /etc/ssh/sshd_config.original && chmod a-w /etc/ssh/sshd_config.original;

if [ -d ~/sshd_config ]
then 
    rm -rf sshd_config;
    git clone https://gitlab.com/DreamsEngine/sshd_config.git ./sshd_config;
else
    git clone https://gitlab.com/DreamsEngine/sshd_config.git ./sshd_config;
fi

mv ./sshd_config/sshd_config /etc/ssh
chmod 664 /etc/ssh/sshd_config;
rm -rf ./sshd_config;

service sshd restart;

echo "====================================";
echo "Let's create SSH-KEY"; 
echo "====================================";

export servername=`hostname`;
ssh-keygen -t rsa -b 4096 -C "$servername"; 
eval "$(ssh-agent -s)"; 

echo "====================================";
echo "Thank you, Now your Webserver is set and Ready"; 
echo "====================================";


echo "====================================";
echo "This is your ssh-key";
echo "====================================";
cat ~/.ssh/id_rsa.pub;
echo "====================================";


echo "====================================";
echo "Compleating Cycle";
echo "====================================";
reboot   
echo "====================================";
echo "We are the sum of our memories. Erasing the memories edged in oneself is the same as loosing oneself.";
echo "Iggy will close, Thank you Nora.";
echo "====================================";
